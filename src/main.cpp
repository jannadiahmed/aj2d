#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include<GL/gl.h>

#include "headers/aj.h"
#include "headers/ajhelper.h"

extern "C" {
	#include<fmod.h>
}

Aj aj;

int screen_width = 800;
int screen_height = 600;

char keys[512];
char mouse_buttons[5];

SDL_Surface *screen;
SDL_Event e;


int running = 1;

FMOD_SYSTEM *sound_system;

FMOD_SOUND* sounds[100];
int NUM_SOUNDS = 0;

FMOD_CHANNEL *sound_channel;
FMOD_CHANNEL *music_channel;
FMOD_CHANNELGROUP *music_group;

void init_sound() {
	FMOD_System_Create(&sound_system);
	FMOD_System_Init(sound_system,32,FMOD_INIT_NORMAL, 0);
}

void sound_play_sound(unsigned int sound_id) {
	FMOD_System_PlaySound(sound_system, sounds[sound_id],NULL, 0, &sound_channel);
	//FMOD_Channel_SetVolume(music_channel,100);
}

void sound_play_music(unsigned int sound_id) {
	FMOD_System_PlaySound(sound_system, sounds[sound_id],NULL, 0, &music_channel);
	//FMOD_Channel_SetVolume(music_channel,100);
}

void sound_play_music_looped(unsigned int sound_id) {
	FMOD_Sound_SetMode(sounds[sound_id],FMOD_LOOP_NORMAL);
	FMOD_System_PlaySound(sound_system, sounds[sound_id],NULL, 0, &music_channel);
	//FMOD_Channel_SetVolume(music_channel,100);
}

unsigned int sound_load_sound(std::string filename) {
	unsigned int sound_id = NUM_SOUNDS;
	FMOD_System_CreateSound(sound_system, filename.c_str(), FMOD_DEFAULT, 0, &sounds[sound_id]);
	NUM_SOUNDS++;
	return sound_id;
}

void sound_free() {
	for(int i=0; i<NUM_SOUNDS; i++) {
		FMOD_Sound_Release(sounds[i]);
	}
	FMOD_System_Release(sound_system);
}


unsigned int load_texture(std::string filename) {
	std::cout << filename << std::endl;
	unsigned int texture;
	SDL_Surface *img;
	img = IMG_Load(filename.c_str());

	glGenTextures(1,&texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

	int texture_format;
	if(img->format->BytesPerPixel == 4) {
		texture_format = GL_RGBA;
	} else {
		texture_format = GL_RGB;
	}

	glTexImage2D(GL_TEXTURE_2D,0,texture_format,img->w,img->h,0,texture_format,GL_UNSIGNED_BYTE,img->pixels);
	SDL_FreeSurface(img);

	return texture;
}

static int push_matrix() {
	glPushMatrix();
	return 0;
}

static int pop_matrix() {
	glPopMatrix();
	return 0;
}

static int load_sound() {
	//const char* filename = luaL_checkstring(L,1);
	std::string filename = aj.interpreter.getStackString(0);
	std::cout << filename << std::endl;
	unsigned int sound_id = sound_load_sound(filename);
	//float volume = luaL_checknumber(L,2);
	
	aj.interpreter.pushStackInteger(sound_id);
	return 1;
}

static int play_sound() {
	//unsigned int sound_id = luaL_checkinteger(L,1);
	unsigned int sound_id = aj.interpreter.getStackInteger(0);
	sound_play_sound(sound_id);
	return 0;
}

static int play_music() {
	//unsigned int sound_id = luaL_checkinteger(L,1);
	unsigned int sound_id = aj.interpreter.getStackInteger(0);
	sound_play_music(sound_id);
	return 0;
}

static int play_music_looped() {
	//unsigned int sound_id = luaL_checkinteger(L,1);
	unsigned int sound_id = aj.interpreter.getStackInteger(0);
	sound_play_music_looped(sound_id);
	return 0;
}

static int load_image() {
	//const char* filename = luaL_checkstring(L,1);
	std::string filename = aj.interpreter.getStackString(0);
	std::cout << filename << std::endl;
	std::cout << "debug1" << std::endl;
	unsigned int texture = load_texture(filename);
	std::cout << "debug2" << std::endl;

	//lua_pushnumber(L,texture);
	aj.interpreter.pushStackInteger(texture);
	std::cout << "debug3" << std::endl;
	return 1;
}

static int free_image() {
	//unsigned int texture = luaL_checkinteger(L,1);
	unsigned int texture = aj.interpreter.getStackInteger(0);
	glDeleteTextures(1,&texture);
	return 0;
}

static int draw_image() {
	/*int texture = luaL_checknumber(L,1);
	float x = luaL_checknumber(L,2);
	float y = luaL_checknumber(L,3);
	float w = luaL_checknumber(L,4);
	float h = luaL_checknumber(L,5);*/

	int texture = aj.interpreter.getStackInteger(0);
	float x = aj.interpreter.getStackFloat(1);
	float y = aj.interpreter.getStackFloat(2);
	float w = aj.interpreter.getStackFloat(3);
	float h = aj.interpreter.getStackFloat(4);

	float xx = (x+w/2)/(screen_width/2)-1;
	float yy = (y+h/2)/(screen_height/2)-1;
	float ww = (screen_width/2 -w/2)/(screen_width/2)-1;
	float hh = (screen_height/2 -h/2)/(screen_height/2)-1;


	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture);
	glTranslatef(xx,yy,0);

	GLfloat rect[] = {
		-ww,-hh,
		ww,-hh,
		ww,hh,
		-ww,hh
	};

	GLfloat uvs[] = {
		1,1,
		0,1,
		0,0,
		1,0
	};
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY_EXT);

	glVertexPointer(2,GL_FLOAT,0,rect);
	glTexCoordPointer(2,GL_FLOAT,0,uvs);
	glDrawArrays(GL_TRIANGLE_FAN,0,4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY_EXT);
	
	
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	return 0;
}

static int draw_image_flipped() {
	//int texture = luaL_checknumber(L,1);
	//float x = luaL_checknumber(L,2);
	//float y = luaL_checknumber(L,3);
	//float w = luaL_checknumber(L,4);
	//float h = luaL_checknumber(L,5);

	int texture = aj.interpreter.getStackInteger(0);
	float x = aj.interpreter.getStackFloat(1);
	float y = aj.interpreter.getStackFloat(2);
	float w = aj.interpreter.getStackFloat(3);
	float h = aj.interpreter.getStackFloat(4);

	float xx = (x+w/2)/(screen_width/2)-1;
	float yy = (y+h/2)/(screen_height/2)-1;
	float ww = (screen_width/2 -w/2)/(screen_width/2)-1;
	float hh = (screen_height/2 -h/2)/(screen_height/2)-1;


	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture);
	glTranslatef(xx,yy,0);

	/*glBegin(GL_QUADS);
		glTexCoord2f(1,1);
		glVertex3f(ww,-hh,0);
		glTexCoord2f(0,1);
		glVertex3f(-ww,-hh,0);
		glTexCoord2f(0,0);
		glVertex3f(-ww,hh,0);
		glTexCoord2f(1,0);
		glVertex3f(ww,hh,0);
	glEnd();*/

	GLfloat rect[] = {
		ww,-hh,
		-ww,-hh,
		-ww,hh,
		ww,hh
	};

	GLfloat uvs[] = {
		1,1,
		0,1,
		0,0,
		1,0
	};
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY_EXT);

	glVertexPointer(2,GL_FLOAT,0,rect);
	glTexCoordPointer(2,GL_FLOAT,0,uvs);
	glDrawArrays(GL_TRIANGLE_FAN,0,4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY_EXT);
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	return 0;
}

static int draw_tile() {
	/*int texture = luaL_checknumber(L,1);

	float x = luaL_checknumber(L,2);
	float y = luaL_checknumber(L,3);
	float w = luaL_checknumber(L,4);
	float h = luaL_checknumber(L,5);

	float tx = luaL_checknumber(L,6);
	float ty = luaL_checknumber(L,7);
	float tw = luaL_checknumber(L,8);
	float th = luaL_checknumber(L,9);*/

	int texture = aj.interpreter.getStackInteger(0);
	float x = aj.interpreter.getStackFloat(1);
	float y = aj.interpreter.getStackFloat(2);
	float w = aj.interpreter.getStackFloat(3);
	float h = aj.interpreter.getStackFloat(4);
	float tx = aj.interpreter.getStackFloat(5);
	float ty = aj.interpreter.getStackFloat(6);
	float tw = aj.interpreter.getStackFloat(7);
	float th = aj.interpreter.getStackFloat(8);

	float xx = (x+w/2)/(screen_width/2)-1;
	float yy = (y+h/2)/(screen_height/2)-1;

	float ww = (screen_width/2-w/2)/(screen_width/2)-1;
	float hh = (screen_height/2-h/2)/(screen_height/2)-1;

	int www,hhh;

	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, texture);

	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &www);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &hhh);

	float txx1 = tx/www;
	float txx2 = (tx+tw)/www;
	float tyy1 = ty/hhh;
	float tyy2 = (ty+th)/hhh;

	glTranslatef(xx,yy,0);

	GLfloat rect[] = {
		ww,-hh,
		-ww,-hh,
		-ww,hh,
		ww,hh
	};

	GLfloat uvs[] = {
		txx1,tyy2,
		txx2,tyy2,
		txx2,tyy1,
		txx1,tyy1
	};
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY_EXT);

	glVertexPointer(2,GL_FLOAT,0,rect);
	glTexCoordPointer(2,GL_FLOAT,0,uvs);
	glDrawArrays(GL_TRIANGLE_FAN,0,4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY_EXT);

	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	return 0;
}


static int image_size() {
	//int texture = luaL_checkinteger(L,1);
	int texture = aj.interpreter.getStackInteger(0);
	int w,h;

	glBindTexture(GL_TEXTURE_2D, texture);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);

	aj.interpreter.pushStackInteger(w);
	aj.interpreter.pushStackInteger(h);

	return 2;
}

static int fill_background() {
	int r = aj.interpreter.getStackInteger(0);
	int g = aj.interpreter.getStackInteger(1);
	int b = aj.interpreter.getStackInteger(2);

	glClearColor(r/255,g/255,b/255,1);

	return 0;
}

static int setColor() {
	int r = aj.interpreter.getStackInteger(0);
	int g = aj.interpreter.getStackInteger(1);
	int b = aj.interpreter.getStackInteger(2);
	float alpha = aj.interpreter.getStackFloat(3);

	glColor4f(r/255,g/255,b/255,alpha);

	return 0;
}

static int draw_rect() {
	float x = aj.interpreter.getStackFloat(0);
	float y = aj.interpreter.getStackFloat(1);
	float w = aj.interpreter.getStackFloat(2);
	float h = aj.interpreter.getStackFloat(3);

	float xx = (x+w/2)/(screen_width/2)-1;
	float yy = (y+h/2)/(screen_height/2)-1;
	float ww = (screen_width/2-w/2)/(screen_width/2)-1;
	float hh = (screen_height/2-h/2)/(screen_height/2)-1;

	glPushMatrix();
	glTranslatef(xx,yy,0);

	/*glBegin(GL_LINE_LOOP);
		glVertex3f(-ww,-hh,0);
		glVertex3f(ww,-hh,0);
		glVertex3f(ww,hh,0);
		glVertex3f(-ww,hh,0);
	glEnd();*/

	GLfloat rect[] = {
		ww,-hh,
		-ww,-hh,
		-ww,hh,
		ww,hh
	};

	glEnableClientState(GL_VERTEX_ARRAY);

	glVertexPointer(2,GL_FLOAT,0,rect);
	glDrawArrays(GL_LINE_LOOP,0,4);

	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();

	return 0;
}

static int fill_rect() {
	float x = aj.interpreter.getStackFloat(0);
	float y = aj.interpreter.getStackFloat(1);
	float w = aj.interpreter.getStackFloat(2);
	float h = aj.interpreter.getStackFloat(3);

	//float xx = ((x+w/2)/screen_width-0.5f) * 2.0f;
	float xx = (x+w/2)/(screen_width/2)-1;
	// ((0+40/2)/400-0.5f) * 2 => 20/400 - 0.5f *2 => 0.05 
	float yy = (y+h/2)/(screen_height/2)-1;

	float ww = (screen_width/2-w/2)/(screen_width/2)-1;
	float hh = (screen_height/2-h/2)/(screen_height/2)-1;

	glPushMatrix();
	glTranslatef(xx,yy,0);
	//glTranslatef(-1+(w/2)/screen_width,yy,0);

	GLfloat rect[] = {
		ww,-hh,
		-ww,-hh,
		-ww,hh,
		ww,hh
	};

	glEnableClientState(GL_VERTEX_ARRAY);

	glVertexPointer(2,GL_FLOAT,0,rect);
	glDrawArrays(GL_TRIANGLE_FAN,0,4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glPopMatrix();


	return 0;
}

static int get_mouse_pos() {
	int x,y;
	SDL_GetMouseState(&x, &y);
	//lua_pushinteger(L,x);
	//lua_pushinteger(L,y);
	return 0;
}

static int get_mouse_button() {
	//int btn = luaL_checkinteger(L,1);
	int btn = aj.interpreter.getStackInteger(0);
	aj.interpreter.pushStackBool(mouse_buttons[btn] == 1);
	mouse_buttons[btn] = 0;
	return 1;
}

static int scale() {
	float x = aj.interpreter.getStackFloat(0);
	float y = aj.interpreter.getStackFloat(1);
	float z = aj.interpreter.getStackFloat(2);
	glScalef(x,y,z);
	return 0;
}

static int rotate() {
	float angle = aj.interpreter.getStackFloat(0);
	float x = aj.interpreter.getStackFloat(1);
	float y = aj.interpreter.getStackFloat(2);
	float z = aj.interpreter.getStackFloat(3);
	glRotatef(angle,x,y,z);
	return 0;
}

static int translate() {
	float x = aj.interpreter.getStackFloat(0);
	float y = aj.interpreter.getStackFloat(1);
	float z = aj.interpreter.getStackFloat(2);
	glTranslatef(x,y,z);
	return 0;
}

static int keydown() {
	int key = aj.interpreter.getStackInteger(0);
	aj.interpreter.pushStackBool(keys[key]==1);
	return 1;
}

static int keypressed() {
	int key = aj.interpreter.getStackInteger(0);
	aj.interpreter.pushStackBool(keys[key]==1);
	keys[key] = 0;
	return 1;
}

void init_gl() {
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

int main(int argc, char** argv) {
	
	AJ_Helper_Init(&aj);

	// configs
	//lua_pushboolean(L,0);
	//lua_setglobal(L,"screen_fullscreen");
	aj.interpreter.setBool("screen_fullscreen",false);
	
	//lua_pushinteger(L,screen_width);
	//lua_setglobal(L,"screen_width");
	aj.interpreter.setInteger("screen_width",screen_width);

	//lua_pushinteger(L,screen_height);
	//lua_setglobal(L,"screen_height");
	aj.interpreter.setInteger("screen_height",screen_height);

	// functions

	//lua_pushcfunction(L,load_sound);
	//lua_setglobal(L,"load_sound");
	aj.interpreter.setC_Function("load_sound",load_sound);


	//lua_pushcfunction(L,play_sound);
	//lua_setglobal(L,"play_sound");

	aj.interpreter.setC_Function("play_sound",play_sound);

	//lua_pushcfunction(L,play_music);
	//lua_setglobal(L,"play_music");
	aj.interpreter.setC_Function("play_music",play_music);

	//lua_pushcfunction(L,play_music_looped);
	//lua_setglobal(L,"play_music_looped");
	aj.interpreter.setC_Function("play_music_looped",play_music_looped);

	//lua_pushcfunction(L,load_image);
	//lua_setglobal(L,"load_image");
	aj.interpreter.setC_Function("load_image",load_image);

	//lua_pushcfunction(L,free_image);
	//lua_setglobal(L,"free_image");
	aj.interpreter.setC_Function("free_image",free_image);

	//lua_pushcfunction(L,draw_image);
	//lua_setglobal(L,"draw_image");
	aj.interpreter.setC_Function("draw_image",draw_image);

	//lua_pushcfunction(L,draw_image_flipped);
	//lua_setglobal(L,"draw_image_flipped");
	aj.interpreter.setC_Function("draw_image_flipped",draw_image_flipped);

	//lua_pushcfunction(L,image_size);
	//lua_setglobal(L,"image_size");
	aj.interpreter.setC_Function("image_size",image_size);

	//lua_pushcfunction(L,draw_tile);
	//lua_setglobal(L,"draw_tile");
	aj.interpreter.setC_Function("draw_tile",draw_tile);

	//lua_pushcfunction(L,draw_rect);
	//lua_setglobal(L,"draw_rect");
	aj.interpreter.setC_Function("draw_rect",draw_rect);

	//lua_pushcfunction(L,fill_rect);
	//lua_setglobal(L,"fill_rect");
	aj.interpreter.setC_Function("fill_rect",fill_rect);

	//lua_pushcfunction(L,fill_background);
	//lua_setglobal(L,"fill_background");
	aj.interpreter.setC_Function("fill_background",fill_background);

	//lua_pushcfunction(L,setColor);
	//lua_setglobal(L,"setColor");
	aj.interpreter.setC_Function("set_color",setColor);

	//lua_pushcfunction(L,get_mouse_pos);
	//lua_setglobal(L,"get_mouse_pos");

	//lua_pushcfunction(L,get_mouse_button);
	//lua_setglobal(L,"get_mouse_button");
	aj.interpreter.setC_Function("get_mouse_button",get_mouse_button);

	//lua_pushcfunction(L,scale);
	//lua_setglobal(L,"scale");
	aj.interpreter.setC_Function("scale",scale);

	//lua_pushcfunction(L,rotate);
	//lua_setglobal(L,"rotate");
	aj.interpreter.setC_Function("rotate",rotate);

	//lua_pushcfunction(L,translate);
	//lua_setglobal(L,"translate");
	aj.interpreter.setC_Function("translate",translate);

	//lua_pushcfunction(L,keydown);
	//lua_setglobal(L,"keydown");
	aj.interpreter.setC_Function("keydown",keydown);

	//lua_pushcfunction(L,keypressed);
	//lua_setglobal(L,"keypressed");
	aj.interpreter.setC_Function("keypressed",keypressed);

	//lua_pushcfunction(L,push_matrix);
	//lua_setglobal(L,"push_matrix");
	aj.interpreter.setC_Function("push_matrix",push_matrix);

	//lua_pushcfunction(L,pop_matrix);
	//lua_setglobal(L,"pop_matrix");
	aj.interpreter.setC_Function("pop_matrix",pop_matrix);

	if(argc < 2) {
		//luaL_loadfile(L,"test.lua");
		aj.doFile("test.aj");
	} else {
		//luaL_loadfile(L,argv[1]);
		aj.doFile(argv[1]);
	}

	//lua_getglobal(L,"screen_fullscreen");
	int flags = SDL_OPENGL;
	int fullscreen = aj.interpreter.getBool("screen_fullscreen");
	//printf("%d \n",fullscreen);

	//lua_getglobal(L,"screen_width");
	//screen_width = lua_tointeger(L,-1);
	int screen_width = aj.interpreter.getInteger("screen_width");

	//lua_getglobal(L,"screen_height");
	//screen_height = lua_tointeger(L,-1);
	int screen_height = aj.interpreter.getInteger("screen_height");
	

	if(fullscreen) {
		flags = flags | SDL_FULLSCREEN;
	}

	screen = SDL_SetVideoMode(screen_width,screen_height,32,flags);

	// initializes sound
	init_sound();

	// initializes opengl
	init_gl();

	// executes the lua init() function
	//luaL_dostring(L,"init()");
	aj.doCommand("init()");


	Uint32 FPS = 0;
	Uint32 now = SDL_GetTicks();
	Uint32 lasttime = now;

	while(running) {
		while(SDL_PollEvent(&e)) {
			if(e.type == SDL_QUIT)
				running = 0;
			if(e.type == SDL_KEYDOWN) {
				keys[e.key.keysym.sym] = 1;
				if(e.key.keysym.sym == SDLK_ESCAPE)
					running = 0;
			}
			if(e.type == SDL_KEYUP) {
				keys[e.key.keysym.sym] = 0;
			}

			if(e.type == SDL_MOUSEBUTTONDOWN) {
				//printf("%d \n",e.button.button);
				mouse_buttons[e.button.button] = 1;
			}
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0,0,0,1);
		//glClearDepth(1);

		glLoadIdentity();
		glScalef(1,-1,1);
		
		// executes the lua render() function
		//luaL_dostring(L,"render()");
		aj.doCommand("render()");

		SDL_GL_SwapBuffers();
		FPS++;
		now = SDL_GetTicks();
		if(now - lasttime >= 1000) {
			printf("%d \n",FPS);
			lasttime = now;
			FPS = 0;
		}

		SDL_Delay(16);
	}

	for (int i=0; i<NUM_SOUNDS; i++) {
		FMOD_Sound_Release(sounds[i]);
	}
	FMOD_System_Release(sound_system);

	SDL_Quit();
	//lua_close(L);
}
