#pragma once

class AJ2D {
	public:
		AJ2D();
		~AJ2D();
	private:
		Screen *Screen;
		Graphics *graphics;
		Sound *sound;
		Input *input;
		Config *config;
};
