# pong.aj
#
# Pong by Ahmed Jannadi


#screen_fullscreen = true

function collision_ball_p1()
	return ball_x < p1_x + p_width and ball_x > p1_x and ball_y < p1_y + p_height and ball_y > p1_y
end

function collision_ball_p2()
	return ball_x < p2_x + p_width and ball_x > p2_x and ball_y < p2_y + p_height and ball_y > p2_y
end

function init()

	p_width = 10
	p_height = 50

	p1_x = 10
	p1_y = screen_height / 2

	p2_x = screen_width - 20
	p2_y = screen_height / 2

	ball_x = screen_width / 2
	ball_y = screen_height / 2
	ball_size = 5

	ball_vx = -5
	ball_vy = 5

	col_sound = load_sound("assets/jump.wav")

end

function render()

	if(ball_x > screen_width or ball_x < 0)
		init()
	end

	if(ball_y > screen_height or ball_y < 0)
		ball_vy = -ball_vy
	end

	if(ball_y < p2_y + 25)
		p2_y = p2_y - 5
	end

	if(ball_y > p2_y + 25)
		p2_y = p2_y + 5
	end
	
	ball_x = ball_x + ball_vx
	ball_y = ball_y + ball_vy

	if(collision_ball_p1() or collision_ball_p2())
		ball_vx = -ball_vx
		play_sound(col_sound)
	end

	if(keydown(119))
		p1_y = p1_y - 5
	end

	if(keydown(115))
		p1_y = p1_y + 5
	end


	draw_rect(p1_x,p1_y,p_width,p_height)
	draw_rect(p2_x,p2_y,p_width,p_height)
	draw_rect(ball_x, ball_y, ball_size, ball_size)
end
