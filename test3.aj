x = 0
y = 0
vx = 0
vy = 0

function init()
	a = load_sound("assets/bgm.mp3")
	m = load_image("assets/mario.png")
	play_music_looped(a)
end

function render()
	y = add(y,vy)
	vy++ 
	if(y+50 > screen_height)
		y = screen_height-50
		vy = 0
	end
	if(keydown(100))
		x = x+5
	end

	if(keydown(97))
		x = x-5
	end
	draw_image(m,x,y,50,50)
end
